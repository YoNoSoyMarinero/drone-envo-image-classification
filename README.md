# Semantic segmentation based on ecological enovirment on drone images

This repository will be utilized to store scripts for feature extraction from drone images, as well as for model training and testing. The motivation behind this project is the necessity for a robust model that can aid in monitoring eco habitats.

We will employ computer vision techniques, specifically deep learning models for semantic segmentation, in this project. Our objective is to identify the optimal architecture that best suits our problem.

The initial phase in constructing this model involves establishing standardized techniques for feature extraction, implementing a systematic approach for data partitioning into training and testing sets, and ensuring the adoption of standardized methods for training. This systematic framework aims to maintain a professional and rigorous methodology throughout the model development process.

## How to install

For package manager we recommend you to use poetry. To install poetry just run following command:

```bash
pip install poetry
```

Afterwards you should clone this repository, cd into it and run poetry install to create virtual envoirment you'll need to run this project.

```bash
git clone https://gitlab.com/YoNoSoyMarinero/drone-envo-image-classification.git
```

```bash
cd drone-envo-image-classification
poetry config virtualenvs.in-project true
poetry install
```

To activate your virtual envoirment run this:

```bash
.venv\Scripts\activate
```

## 1. Feature extraction

Current idea is to classify each pixel of drone images into 6 six segments based on type of envoirment: Tree canopy, Water surface, Grass, Soil, Crop fields and Water vegetation.

### 1.1. MaskMaker class

- Gets paths from drone image, shape file and output mask and gets number of classes. Based on that it creates
  masks for coresponding shape files with same size as .tiff drone images.

### 1.1. ImageCroper class

- Crops images and masks and stores them in seperate directories. Discards images that don't have any information in them.

## 2. Train and test deep learning model

#### TODO

Plan is to use https://github.com/open-mmlab/mmsegmentation toolbox to fine tune model and use it later on.
