import geopandas as gpd
from rasterio.features import geometry_mask
import numpy as np
import rasterio

class MaskMaker:
    """
    MaskMaker class combines original georeferenced image and ground truth shape file into gray scale 2D mask 
    """

    __meta: dict = None
    __polygons: gpd.GeoDataFrame = None

    def __init__(self, original_image_path:str, 
                       shape_file_path:str,
                       output_mask_path:str,
                       number_of_classes: int) -> None:
        """
        original_image_path:str - Path to original image
        shape_file_path:str - Path to ground truth shape file
        output_mask_path:str - Path to where you want to save your ground truths mask
        number_of_classes: int - Number of classes
        """
        
        self.__original_image_path: str = original_image_path
        self.__shape_file_path: str = shape_file_path
        self.__output_mask_path: str = output_mask_path
        self.__colors: list[int] = np.linspace(1, number_of_classes, number_of_classes).astype("uint8")
        self.__load_geodataframe()
        self.__load_tif_image_info()
        
    def __load_geodataframe(self) -> None:
        """
        Loads geodataframe
        """
        self.__polygons = gpd.read_file(self.__shape_file_path)

    def __load_tif_image_info(self) -> None:
        """
        Loads tif image information
        """
        with rasterio.open(self.__original_image_path, 'r') as src:
            self.__meta = src.meta

    def __save_mask(self, mask: np.array) -> None:
        """
        Saves newly created mask
        """
        with rasterio.open(self.__output_mask_path, 'w', driver=self.__meta['driver'], width=self.__meta['width'], height=self.__meta['height'], count=1, dtype=np.uint8, crs=self.__meta['crs'], transform=self.__meta['transform']) as dst:
            dst.write(mask, 1)

    def make_mask(self) -> None:
        """
        Iterates over all poygons inside ground truth shape file while creating mask image based on class of current polygon
        """
        
        shape: tuple[int] = (self.__meta['width'], self.__meta['height'])
        self.__polygons = self.__polygons.to_crs(crs=self.__meta['crs'])
        image_mask: np.array = np.zeros(shape)
        for _, row in self.__polygons.iterrows():
            polygon = row['geometry']
            mask: np.array = geometry_mask(gpd.GeoSeries(polygon), out_shape=shape, transform=self.__meta['transform'], invert=True)
            image_mask[mask] = self.__colors[row['id']]

        self.__save_mask(image_mask)