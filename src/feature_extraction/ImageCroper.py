import numpy as np
import matplotlib.pyplot as plt

class ImageCroper:

    def __init__(self, path_to_images: str = None, path_to_labels: str = None) -> None:
        self.path_to_images: str = path_to_images
        self.path_to_labels: str = path_to_labels
        self.patch_size: int = 512


    def crop_image_generator(self, image: np.array, mask: np.array, name: str) -> None:
        """
        Args:
            image: np.array
            mask: np.array
            name: str - Name of current image

        Returns:
            None 
        """

        height, width, _ = image.shape
        for y in range(0, height, self.patch_size):
            for x in range(0, width, self.patch_size):

                patch_height: np.array = min(self.patch_size, height - y)
                patch_width: np.array = min(self.patch_size, width - x)
                image_patch: np.array = image[y:y+patch_height, x:x+patch_width, :3]
                mask_patch: np.array = mask[y:y+patch_height, x:x+patch_width]

                if mask_patch.min() != 0 and patch_height == self.patch_size and patch_width == self.patch_size:
                    self.__save_to_dataset(image_patch, mask_patch, y, x, name)

    def display_patches(self, image_patch: np.array, mask_patch: np.array) -> None:
        """
        Displays mask and path

        Args:
        image_patch: str
        mask_patch: str

        Returns:
        None
        """
        fig, axs = plt.subplots(1, 2, figsize=(10, 5))
        axs[0].imshow(image_patch)
        axs[0].set_title('Image Patch')
        axs[1].imshow(mask_patch, cmap='viridis', vmin=mask_patch.min(), vmax=mask_patch.max())
        axs[1].set_title('Mask Patch')

        plt.show()

    def __save_to_dataset(self, image: np.array, label: np.array, y: int, x: int, name: str):
        """
        Saves image and label to corresponding folder

        Args:
        image_path: str
        mask_path: str
        name: str

        Returns: 
        None
        """
        image_filename = f"{name}_{y}_{x}_image.jpg"
        mask_filename = f"{name}_{y}_{x}_mask"

        image_path = f"{self.path_to_images}/{image_filename}"
        plt.imsave(image_path, image)

        mask_path = f"{self.path_to_labels}/{mask_filename}"
        np.save(mask_path, label)

        print(f"Saved patches to: \nimage: {image_path} \nmask: {mask_path}")
